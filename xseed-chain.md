---
description: Our own layer 1 blockchain forked from Ethereum
cover: .gitbook/assets/mxs_banner_nologo_1500_300.png
coverY: 0
layout:
  cover:
    visible: true
    size: hero
  title:
    visible: true
  description:
    visible: true
  tableOfContents:
    visible: true
  outline:
    visible: true
  pagination:
    visible: true
---

# XSeed Chain

<figure><img src=".gitbook/assets/image (13).png" alt=""><figcaption></figcaption></figure>

<mark style="color:red;">**XSeed Chain**</mark> <mark style="color:red;"></mark><mark style="color:red;">is a new layer-1 protocol</mark> based on a delegated proof of stake (DPoS) consensus mechanism designed and innovated to offer substantive performance benefits for game developers including greater throughput and faster completion times, while saving attractive censorship-resistance characteristics.

As a game studio, we will naturally have a large number of games eventually creating millions of transactions. In order to provide the best possible player experience, it is essential that we minimise transaction fees, have the ability to increase TPS and scale up on demand. Having our own Layer 1 allows us to set the parameters that best suits our games and partners.

The reason we have our own chain:

1. Being a fork of Ethereum, we get the best features from a mature chain and can optimise parameters for gaming, e.g. TPS.
2. We are able to control the gas fees and reward node validators.
3. It is an industry standard for a web3 games studios to have their own chain as demonstrated by competitors such as Gala and Immutable-X.
4. Onboard third party games to create extra utility for our native token.
5. Remove strong dependency on an underlying third party chain. Using Terra Luna as an example, it is risky to be fully dependant on a third party technologies to drive the future of a project.&#x20;
6. Allows us to easily create APIs and SDKs faster and manage version control.  We are able to synchronise the roadmap for game development and blockchain.

The XSeed Chain is already up an running in test beta:

## [https://explorer.xseedscan.io/](https://explorer.xseedscan.io/)

<figure><img src=".gitbook/assets/image (14).png" alt=""><figcaption></figcaption></figure>

<figure><img src=".gitbook/assets/image (5) (1).png" alt=""><figcaption></figcaption></figure>

Layer 1 blockchains have distinct functionalities such as the ability to process and finalize transactions on its own chain. As the dedicated main network within our ecosystem, we can also define the underlying rules as well as react fast to any issues that come up. The chain is able to expand to accommodate many forms of secure peer-to-peer and microtransactions, currencies, DeFi, NFTs, DAOs, games, and more.

In future, MetaXSeed Games also have the flexibility to add in layer-2’s for larger gaming ecosystem that require it. These can utilise scaling solutions such as zero-knowledge rollup (zk-rollup) that moves computation and state off-chain into off-chain networks while storing transaction data on-chain on a MetaXSeed layer-1 network.

## [Master Node Sales Deck](https://docs.google.com/presentation/d/1-0Q0YT3xQZk7fOoLx47VLqmRqAn0UmGzi8r2fpP8FfI/edit#slide=id.g259b7af21e0\_0\_30)

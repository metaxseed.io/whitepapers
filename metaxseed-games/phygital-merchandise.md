---
description: Physical + Digital = Phygital
cover: ../.gitbook/assets/mxs_banner_nologo_1500_300.png
coverY: 0
layout:
  cover:
    visible: true
    size: hero
  title:
    visible: true
  description:
    visible: true
  tableOfContents:
    visible: true
  outline:
    visible: true
  pagination:
    visible: true
---

# Phygital Merchandise

<figure><img src="../.gitbook/assets/image (18).png" alt=""><figcaption></figcaption></figure>

As MetaXSeed Games owns the intellectual properties to 32+ games, the characters and game assets can be used to print and sell tangible goods to consumers.  Eventual plan is to <mark style="color:red;">**print NFTs directly onto products**</mark>.

The term **Phygital** comes from the fusion of the words physical and digital – the integration between the physical and digital worlds. It means the incorporation of digital functionalities within the gamer’s physical experience.

Phygital NFTs are non-fungible tokens connected to a physical item. They represent traditional physical goods like collectibles or art pieces, allowing our studio and developers to expand the applications of NFTs to real-world experiences, objects, products, and services.

<figure><img src="../.gitbook/assets/image (22).png" alt=""><figcaption></figcaption></figure>

<figure><img src="../.gitbook/assets/image (23).png" alt=""><figcaption></figcaption></figure>

{% embed url="https://shop.metaxseed.io/" %}

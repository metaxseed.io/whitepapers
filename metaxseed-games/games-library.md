---
description: 30+ Games Published and Ready to Play
cover: ../.gitbook/assets/mxs_banner_nologo_1500_300.png
coverY: 0
layout:
  cover:
    visible: true
    size: hero
  title:
    visible: true
  description:
    visible: true
  tableOfContents:
    visible: true
  outline:
    visible: true
  pagination:
    visible: true
---

# Games Library

Our games studio has a diverse portfolio of games that cater to a wide audience. From action-packed adventures to strategic simulations, we have something for everyone. Our games are available on multiple platforms, including <mark style="color:red;">Steam</mark>, <mark style="color:red;">Google Play</mark> and our own launcher, making it easy for players to access and enjoy them on their preferred device. With a commitment to creating high-quality, entertaining games, we strive to push the boundaries of what is possible in the world of web3 gaming.

<figure><img src="../.gitbook/assets/mxs_banner_nologo_2048_1152.png" alt=""><figcaption></figcaption></figure>

## <mark style="color:blue;background-color:blue;">Play Our Games</mark>

<figure><img src="../.gitbook/assets/image (26).png" alt=""><figcaption></figcaption></figure>

#### Games published on Steam (Beta)

{% embed url="https://store.steampowered.com/curator/43163611-metaxseed/" %}

#### Games published on Google Play (Beta)

{% embed url="https://play.google.com/store/apps/developer?id=MetaXSeed+Games" %}


---
description: >-
  Gaming hub from which all our games will be launched.  Sign in once and play
  all our games.
cover: ../.gitbook/assets/mxs_banner_nologo_1500_300.png
coverY: 0
layout:
  cover:
    visible: true
    size: hero
  title:
    visible: true
  description:
    visible: true
  tableOfContents:
    visible: true
  outline:
    visible: true
  pagination:
    visible: true
---

# Games Launcher

<figure><img src="../.gitbook/assets/image (16).png" alt=""><figcaption></figcaption></figure>

A single location from which all of our games will be accessible. A games launcher has the following advantages:

1. No dependency on third party services to distribute games
2. All player engagement data are easily accessible
3. Cross promotion of games
4. Additional ad revenues

<figure><img src="../.gitbook/assets/image (17).png" alt=""><figcaption></figcaption></figure>

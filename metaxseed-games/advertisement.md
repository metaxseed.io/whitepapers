---
description: Proven revenue model to ensure long term sustainability
cover: ../.gitbook/assets/mxs_banner_nologo_1500_300.png
coverY: 0
layout:
  cover:
    visible: true
    size: hero
  title:
    visible: true
  description:
    visible: true
  tableOfContents:
    visible: true
  outline:
    visible: true
  pagination:
    visible: true
---

# Advertisement

<figure><img src="../.gitbook/assets/image (19).png" alt=""><figcaption></figcaption></figure>

In-game advertising is a commonly used monetization strategy that we will use to boost the ecosystem revenues. Intrinsic in-game advertising helps reach target users in a non-disruptive, highly-viewable environment. Advertisers are able to connect brands with a captive audiences who are unlikely to be multitasking, which increases the likelihood of brand recall and awareness.

We estimate $50M-$100M annual revenue based on industry average pricing and conservative DAU (daily active users) numbers.  This sustainable income generation will help fund player rewards, operation and creation of new games.

All of our games will remain free-to-play ensuring there is a low barrier to entry and opportunity for all players to be rewarded simply by playing and having fun.

<figure><img src="../.gitbook/assets/image (21).png" alt=""><figcaption></figcaption></figure>

<figure><img src="../.gitbook/assets/game_3.png" alt=""><figcaption></figcaption></figure>

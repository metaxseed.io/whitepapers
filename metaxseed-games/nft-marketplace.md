---
description: Buy/Sell gaming NFTs which will utilize our native token
cover: ../.gitbook/assets/mxs_banner_nologo_1500_300.png
coverY: 0
layout:
  cover:
    visible: true
    size: hero
  title:
    visible: true
  description:
    visible: true
  tableOfContents:
    visible: true
  outline:
    visible: true
  pagination:
    visible: true
---

# NFT Marketplace

<figure><img src="../.gitbook/assets/image (15).png" alt=""><figcaption></figcaption></figure>

An NFT marketplace is an online platform where you can buy, sell, auction, and mint non-fungible tokens.  Gamers have the opportunity to buy, sell, and trade gaming items that are add-ons to what the game offers. You can trade the collectible assets with other players.

Our NFT marketplace is currently being tested and can be accessed via the link below.

{% embed url="https://market.metaxseed.io/" %}

### How Does NFT Marketplaces Create Revenue? <a href="#how-does-nft-marketplaces-makes-money" id="how-does-nft-marketplaces-makes-money"></a>

\
The primary revenue-driving factor for an NFT Marketplace is its fees structure.  Every marketplace platform charges some set of fees for every activity happening within the platform. Now let us have a look at such charges that are mandatory in several popular NFT Marketplace platforms.

* NFT Minting
* Listing Fees
* Transaction Fees

**NFT Minting -** NFT minting is simply a process of converting any kind of asset into a new digital asset that is represented in the form of a non-fungible tokens. Only by doing this an asset can be traded in an NFT Marketplace.

To do so, the user has to take their desired asset and convert it into an NFT with the help of NFT Marketplace. For this activity, the marketplace platform could optionally charge a certain amount of fees from the user.

**Listing Fees -** After creating any kind of assets like a photo, artwork, video, music etc into an NFT, the user has to showcase their NFT in the marketplace platform so that the interested buyers could make a bid on that particular asset. In order to list that NFT, the platform would charge a listing fee from the user.

**Transaction Fees -** Once an NFT has been listed out and open for a sale, then several interested buyers would make a bid on that NFT. When the owner of the NFT agrees to trade the NFT to a specific bid and the buyer intends to make the payment, a percentage of 2 to 2.5 from the final sale price of the NFT will be charged by the platform as a transaction fee.

---
cover: ../../.gitbook/assets/mxs_banner_nologo_1500_300.png
coverY: 0
layout:
  cover:
    visible: true
    size: hero
  title:
    visible: true
  description:
    visible: true
  tableOfContents:
    visible: true
  outline:
    visible: true
  pagination:
    visible: true
---

# Monetization

<figure><img src="../../.gitbook/assets/7.png" alt=""><figcaption></figcaption></figure>

Each of the games developed or incubated within the ecosystem will have a set of NFTs for various digital assets such as land, characters, weapons, skins, power-ups, etc.  $XSEED holders will get early access to the genesis NFT collections for the games and also benefit from marketplace profit share.  NFT sales will provide a long term and sustainable supplementary income in addition to the initial tokens sales of the games.&#x20;

<figure><img src="../../.gitbook/assets/8.png" alt=""><figcaption></figcaption></figure>

Being a game studio, the MetaXSeed marketplace will be a hub for game assets transactions for all of our games.  This should result in significant transaction revenues generated that can be utilised for many different purposes all designed to benefit the XSEED token holders.

<figure><img src="../../.gitbook/assets/13.png" alt=""><figcaption></figcaption></figure>

MetaXSeed plan to create a set of 'Out of the Box (OOTB)' solutions & frameworks.  These are ready-made programs with sought after functionality to increase productivity and reduce cost. These will be targeted at all game makers and aim to solve common problems introducing standard processes for the entire p2e gaming industry.&#x20;

**Pros of OOTB**

* Most **inexpensive** software type on the market.
* Developed based on market analysis and **industry needs**.&#x20;
* Support and **constant updates** are often included by out of the box.
* Being a widely used solution, the software might have a huge **user-base**.
* Unlike other software types, OOTB needs **minimal effort** to set up and deploy.

All of these advantages of OOTB means that MetaXSeed will be in a good position to create great partnerships and charge a licence fees for vital infrastructure components that handle security, performance and game-fi.

<figure><img src="../../.gitbook/assets/4 (1).png" alt=""><figcaption></figcaption></figure>

Every game that is deployed within a metaverse like NetVrk, Bloktopia or our own **MetaX**, will be able to leverage advertising opportunities located within the land they reside in.  Within the metaverse, games will be able generate income by offering brands the following:

* #### Virtual reality billboards
* #### Sponsored content in social spaces
* #### Product placement in VR games
* #### A new generation of influencers
*   #### Immersive native ad experiences



    <figure><img src="../../.gitbook/assets/image (5).png" alt=""><figcaption></figcaption></figure>

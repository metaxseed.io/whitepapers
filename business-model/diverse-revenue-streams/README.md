---
cover: ../../.gitbook/assets/mxs_banner_nologo_1500_300.png
coverY: 0
layout:
  cover:
    visible: true
    size: hero
  title:
    visible: true
  description:
    visible: true
  tableOfContents:
    visible: true
  outline:
    visible: true
  pagination:
    visible: true
---

# Diverse Revenue Streams

As we own the intellectual properties (games, characters, in-game digital billboards) for over 32 games, it creates a unique opportunity to be able to extract value at every stage and a diverse range of revenue streams which include:

1. Advertising
2. NFT Sales
3. NFT Marketplace Transaction
4. Blockchain Related Services
5. Merchandise Sales
6. Player-to-Entity Micro Transaction
7. SDK Licensing

Each of these streams provides a unique source of income, allowing us to stay ahead of the curve in the fast-paced world of web3 gaming. Whether it's through the sale of one-of-a-kind NFTs, the licensing of our cutting-edge technology, or the monetization of our vast metaverse, we are constantly exploring new ways to drive growth and create value for our stakeholders.


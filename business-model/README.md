---
cover: ../.gitbook/assets/mxs_banner_nologo_1500_300.png
coverY: 0
layout:
  cover:
    visible: true
    size: hero
  title:
    visible: true
  description:
    visible: true
  tableOfContents:
    visible: true
  outline:
    visible: true
  pagination:
    visible: true
---

# Business Model

MetaXSeed Games business model that incorporates community voting and revenue sharing allows the players to have a direct impact on the development of the games and to benefit financially from the success of the games.&#x20;

In this model, the community can vote on the games they would like to see developed, and the game studio will use these votes to prioritize their development pipeline. The revenue generated from these games is then shared with the community, giving players a direct stake in the success of the games they helped shape. This model creates a strong incentive for players to engage with and support the games, driving the growth and success of the studio.


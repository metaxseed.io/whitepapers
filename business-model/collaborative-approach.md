---
cover: ../.gitbook/assets/mxs_banner_nologo_1500_300.png
coverY: 0
layout:
  cover:
    visible: true
    size: hero
  title:
    visible: true
  description:
    visible: true
  tableOfContents:
    visible: true
  outline:
    visible: true
  pagination:
    visible: true
---

# Collaborative Approach

A collaborative business model emphasizes teamwork and cooperation among stakeholders to achieve a common goal. In the context of game development, this model aims to ensure that the tools and frameworks being developed have the right market fit and can be used by a wide range of games. The goal is to increase productivity and reduce time to market. By working closely with game developers, designers, and other relevant parties, the business can understand their needs and create solutions that are tailored to their specific requirements. This approach leads to better results, faster development times, and ultimately, more successful games.

### <mark style="color:blue;background-color:blue;">Challenges of creating AA or AAA Games</mark>

Shawn Layden, a former executive for PlayStation recently mentioned that the average budgets of a "AAA" game is around between $80million and $150million on average, remarking that “the problem with that model is it’s just not sustainable.”  Furthermore, in another recent interview, Sony Computer Entertainment CEO Andrew House stated  “AAA games need to be generally shorter, more sustainable, and they are more creative. And I think when you try to do that on every single title, it ends up not being as good as it can be.”

Cost and lack of talent are the two main limiting factors that cause blockchain games to be of low quality.  We plan to tackle both problems by taking ideas from pioneers such as SpaceX:

1. Maximise re-usability of underlying infrastructure components
2. Hire the best talent and train the next generation of game developers and 3D artists specialized in blockchain gaming.  There will be a comprehensive in-house training programme for graduates.

### <mark style="color:blue;background-color:blue;">What can MetaXSeed offer Game Creators?</mark>

In addition to creating our own games games, MetaXSeed will be able to service our partners with range B2B service offerings which can be exchanged for either tokens or a licence fee.  Some of these modularised complements include..

|                                                         |                                                                                            |
| ------------------------------------------------------- | ------------------------------------------------------------------------------------------ |
| <mark style="color:green;">Game Development</mark>      | Storyboarding, Characters Creation, Lore, Game Mechanics, Testing, Deployment, Rebalancing |
| <mark style="color:green;">Digital Asset Library</mark> | 3D Characters, Texturing, Materials, Rigging, Animations, Environments, Physics, VFX       |
| <mark style="color:green;">Game Infrastructure</mark>   | Security, Performance, Scalability                                                         |
| <mark style="color:green;">Integrations API</mark>      | Cross Chain Support, Wearables API                                                         |
| <mark style="color:green;">Game-Fi & De-Fi</mark>       | Staking, Yield Farming, NFT Mining, Credit                                                 |
| <mark style="color:green;">Metaverse & Land</mark>      | Access to METAX metaverse                                                                  |
|                                                         |                                                                                            |


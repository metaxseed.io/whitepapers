---
cover: .gitbook/assets/mxs_banner_nologo_1500_300.png
coverY: 0
layout:
  cover:
    visible: true
    size: hero
  title:
    visible: true
  description:
    visible: true
  tableOfContents:
    visible: true
  outline:
    visible: true
  pagination:
    visible: true
---

# Our Games Pipeline

MetaXSeed has 32+ games in various stages of development. We aim to make games fun to play in order to attract players from the worldwide pool of 3 billion gamers. Sustainable revenue models such as advertisement will ensure all our games remain free to play while also providing strong incentives to participate within our ecosystem.&#x20;

{% embed url="https://www.youtube.com/watch?v=8G_uswF1kEk" %}

<figure><img src=".gitbook/assets/mxs_banner_nologo_1920_1080 (1).png" alt=""><figcaption></figcaption></figure>

---
cover: .gitbook/assets/mxs_banner_nologo_1500_300.png
coverY: 0
layout:
  cover:
    visible: true
    size: hero
  title:
    visible: true
  description:
    visible: true
  tableOfContents:
    visible: true
  outline:
    visible: true
  pagination:
    visible: true
---

# Unreal Engine 5

MetaXSeed shall be primarily use Unreal Engine 5 because of its cutting-edge technology, powerful tools, and vast community support. UE5 offers advanced features such as photorealistic graphics, real-time lighting, and dynamic physics, which allow us to create high-quality, immersive gaming experiences. The engine's modular architecture and integrated development environment simplify the creation and deployment process, streamlining our workflow and freeing up more time for us to focus on the creative aspects of game development. Additionally, Unreal Engine 5's support for web3 technologies such as blockchain and non-fungible tokens (NFTs) opens up new opportunities for player ownership, scarcity, and monetization within our games.

UE5 offers some ground breaking new technologies that will reshape future gaming which include Nanites & Lumen.

<figure><img src=".gitbook/assets/17.png" alt=""><figcaption></figcaption></figure>

"One of its major features is Nanite, an engine that allows for high-detailed photographic source material to be imported into games.  The Nanite virtualized geometry technology allows Epic to take advantage of its past acquisition of Quixel, the world's largest photogrammetry library as of 2019. The goal of Unreal Engine 5 was to make it as easy as possible for developers to create detailed game worlds without having to spend excessive time on creating new detailed assets." - _Unreal Engine_

<figure><img src=".gitbook/assets/18.png" alt=""><figcaption></figcaption></figure>

"Lumen is Unreal Engine 5’s new dynamic Global Illumination and reflections system, targeted at next-generation consoles. It is the result of years of work from the team at Epic to bring real-time Global Illumination and reflections to Unreal Engine. " - _Unreal Engine_

{% embed url="https://www.youtube.com/watch?v=qC5KtatMcUw" %}
Source = Unreal Engine YouTube Channel
{% endembed %}

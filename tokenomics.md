---
cover: .gitbook/assets/mxs_banner_nologo_1500_300.png
coverY: 0
layout:
  cover:
    visible: true
    size: hero
  title:
    visible: true
  description:
    visible: true
  tableOfContents:
    visible: true
  outline:
    visible: true
  pagination:
    visible: true
---

# Tokenomics

Our tokenomics model is designed with long-term sustainability in mind. A key aspect of this is the implementation of long vesting schedules, which align the interests of token holders with the project's success over time. This promotes stability and encourages continued engagement. Additionally, the burning of NFT transaction fees helps to maintain the token's value by reducing its overall supply. \
\
Furthermore, we are utilizing AI modelling to develop a balanced game-economy ecosystem. This allows us to dynamically adjust key metrics such as token inflation and reward structures in real-time, ensuring that the ecosystem remains healthy and fair for all participants. The combination of these elements creates a virtuous cycle of value creation, increased demand, and liquidity that is essential for long-term sustainability.

<figure><img src=".gitbook/assets/11.png" alt=""><figcaption></figcaption></figure>

1. Max Token Supply = 50 Billion
2. Initial Market Cap at TGE = $270k (extremely low)
3. Total Raise = $4.6 Million
4. Fully Diluted Market Cap = $39 Million
5. Team tokens locked for 12 months
6. Tokens diluting slowly over 11 years
7. Very high token utility to create demand.  See details [here](token-utility.md)

<figure><img src=".gitbook/assets/image.png" alt=""><figcaption></figcaption></figure>

---
cover: .gitbook/assets/mxs_banner_nologo_1500_300.png
coverY: 0
layout:
  cover:
    visible: true
    size: hero
  title:
    visible: true
  description:
    visible: true
  tableOfContents:
    visible: true
  outline:
    visible: true
  pagination:
    visible: true
---

# Roadmap

Our project roadmap is ambitious but achievable, as it will be executed in stages and is highly flexible. The team has a thorough understanding of how to deliver on the roadmap, which will allow us to overcome any challenges along the way. The stages have been carefully planned to ensure that each one builds upon the previous, leading to a successful outcome. This approach also allows for flexibility, as we can make changes and adjustments as needed. With a dedicated team, clear goals, and a strong understanding of the roadmap, we are confident that we will achieve our ambitious objectives.

|             |                                                                                                                                                                                                                                                           |
| ----------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 2021 - 2023 | <p>Studio Games Development</p><p>Games on Steam</p><p>Games on Google Play </p><p>Game Launcher Developed</p><p>Ad-Inject Developed</p><p>Merch Shop Live </p><p>Single Sign On</p>                                                                      |
| 2024 - H1   | <p>Game NFT Sales </p><p>Launch Game - Op Satoshi </p><p>TGE </p><p>Blockchain Mainnet </p><p>Staking Live </p><p>AI Beta Testing </p><p>Launch Game - Seeker</p>                                                                                         |
| 2024 - H2   | <p>NFT Marketplace Live </p><p>Ad-Inject Live </p><p>Launch Game - Shadow Syndicate </p><p>Partner Game Publishing </p><p>AI Feature Integrations </p><p>Launch Game -Jungle Fever </p><p>NFT Merch</p>                                                   |
| 2025        | <p>Launch Game - Odins Ring </p><p>Launch Game - Age of Dragons </p><p>DEX Game Services </p><p>Launch Game - XSpace </p><p>Launch Game - Block Football </p><p>Launch Game - X-Speed </p><p>Launch Game - Defend the Base </p><p>AI Game Development</p> |

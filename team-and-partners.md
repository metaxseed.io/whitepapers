---
description: World class team with a proven track record
cover: .gitbook/assets/mxs_banner_nologo_1500_300.png
coverY: 0
layout:
  cover:
    visible: true
    size: hero
  title:
    visible: true
  description:
    visible: true
  tableOfContents:
    visible: true
  outline:
    visible: true
  pagination:
    visible: true
---

# Team and Partners

Our team is made up of highly skilled professionals who bring a unique blend of expertise from web2 gamers, web3 veterans and software engineering best practices. This combination of skills allows us to offer a unique perspective and approach to solving complex problems that have prevented mass adoption of blockchain gaming. &#x20;

<figure><img src=".gitbook/assets/image (27).png" alt=""><figcaption><p>Team has worked for some of the top organisations in the world</p></figcaption></figure>

<figure><img src=".gitbook/assets/partner_altura copy 4.png" alt=""><figcaption></figcaption></figure>

<table><thead><tr><th width="258">Name</th><th>Experience</th></tr></thead><tbody><tr><td><a href="https://www.linkedin.com/in/shipon-chowdhury-1a669118/">SHIPON CHOWDHURY</a></td><td><h3>CEO, LEAD GAME DEVELOPER</h3><p>20+ years experience within software development, web3 gaming, computing and finance delivering complex solutions for industry leading organisations. Avid gamer and actively developing high quality games in Unreal Engine. Leading a team of world class game developers to create the next generation of blockchain games. </p></td></tr></tbody></table>

<figure><img src=".gitbook/assets/partner_altura copy 7.png" alt=""><figcaption></figcaption></figure>

<table><thead><tr><th width="272">Name</th><th>Experience</th></tr></thead><tbody><tr><td><a href="https://www.linkedin.com/in/max-krawiec-05988814/"><mark style="color:blue;">MAX KRAWEIC</mark></a></td><td><h3>CHIEF TECHNOLOGY OFFICER (CTO)</h3><p>Long term experience in the crypto space mining Ethereum. Specifically focused on web3 game development using unreal engine and blender.  Currently working for Phantom. </p></td></tr></tbody></table>

<figure><img src=".gitbook/assets/partner_altura copy 6.png" alt=""><figcaption></figcaption></figure>

<table><thead><tr><th width="271">Name</th><th>Experience</th></tr></thead><tbody><tr><td><a href="https://www.linkedin.com/in/garibaldymukti/">GARIBALDY MUKTI</a></td><td><h3>CHIEF PRODUCT OFFICER</h3><p>Founder and CEO of the highly successful Night Spade Games Studio. Highly experienced and talented games designer and developer. Fifteen years in the gaming industry working with some of the leading brands in the world such as Google, Microsoft, Nickelodeon and Danone. Gerry will be heading up the Unity division and manage co-ordination of our Asian development teams.</p></td></tr></tbody></table>

<figure><img src=".gitbook/assets/partner_altura copy 5.png" alt=""><figcaption></figcaption></figure>

<table><thead><tr><th width="260">Name</th><th>Experience</th></tr></thead><tbody><tr><td><a href="https://www.linkedin.com/in/timothykingery/">TIM KINGERY</a></td><td><h3>CHIEF STRATEGY OFFICER (CSO)</h3><p>An “O.G.” of the Web3 arena, Tim Kingery has spent the last 6 years helping or leading blockchain start-ups with marketing strategy, business development, and tokenomics. President of Action Force LLC, a US-based GameFi and Tokenomics agency.</p></td></tr></tbody></table>

<figure><img src=".gitbook/assets/partner_altura copy 9.png" alt=""><figcaption></figcaption></figure>

<table><thead><tr><th width="263">Name</th><th>Experience</th></tr></thead><tbody><tr><td><a href="https://www.linkedin.com/in/nathan-christian-90365414a/">NATHAN CHRISTIAN</a></td><td><h3>ADVISOR &#x26; CEO OF LEDGERSCORE</h3><p>Nathan is the Founder and CEO of LedgerScore. A top-rated global influencer in Blockchain and Fintech. Nathan’s a technical expert in blockchain-based accounting and financial applications. An active angel investor and serial entrepreneur with contributions to 40+ technology start-ups</p></td></tr></tbody></table>

<figure><img src=".gitbook/assets/partner_altura copy 10.png" alt=""><figcaption></figcaption></figure>

<table><thead><tr><th width="267">Name</th><th>Experience</th></tr></thead><tbody><tr><td><a href="https://www.linkedin.com/in/rishi-jayanthan-678137159/">RISHI JAYANTHAN</a></td><td><h3>UNREAL ENGINE DEVELOPER</h3><p>Highly proficient game developer bringing proven history of successfully utilizing latest technologies to create exciting games. Rishi will be heading up the unreal engine game division focussing on our flagship games.</p></td></tr></tbody></table>

<figure><img src=".gitbook/assets/partner_altura copy 8.png" alt=""><figcaption></figcaption></figure>

<table><thead><tr><th width="269">Name</th><th>Experience</th></tr></thead><tbody><tr><td><a href="https://www.linkedin.com/in/sundeep-krishna-siripurapu-95575862/">SUNDEEP SIRIPURAPU</a></td><td><h3>ADVISOR - TOKENOMICS</h3><p>Core Team Member of TrustSwap. Designed tokenomics for various projects and/or serving as a strategic advisor/consultant for over 20+ projects encompassing DeFi, GameFi and Layer-1 protocols. Working TO accelerate fundraising along with strategic partnerships through his network of connections.</p></td></tr></tbody></table>

<table><thead><tr><th width="275">Name</th><th>Experience</th></tr></thead><tbody><tr><td><a href="https://www.linkedin.com/in/davidmola/">DAVID MOLA</a></td><td><h3>WEB3 GAME DEVELOPER</h3><p>David is an experienced android engine and frontend developer who has worked on building complex android games, websites and web3 dapps.</p></td></tr></tbody></table>

## <mark style="color:blue;">Specialised Web2 + Web3 Game Developers</mark>&#x20;

Our experienced Web2 + Web3 game developers have a deep understanding of both traditional and the cutting edge decentralized technologies. They are well-versed in programming languages such as C++, JavaScript, HTML, and CSS, as well as blockchain platforms like Ethereum and smart contract development using Solidity. With their knowledge and skills, they are able to create engaging and immersive gaming experiences that take advantage of the unique features of the Web3 ecosystem, such as non-fungible tokens and decentralized finance. They are also familiar with the latest tools and frameworks for game development, including Unity and Unreal Engine. They have worked on a variety of game genres, including RPG, FPS, MMO and have a proven track record of delivering high-quality products on tim

## <mark style="color:blue;">Partnerships</mark>

MetaXSeed Games has a solid set of early investors and also working with industry leading technical partners.

<figure><img src=".gitbook/assets/image (7).png" alt=""><figcaption></figcaption></figure>

---
description: >-
  Complete web3 games studio with a large library of developed games, layer 1
  blockchain and NFT marketplace. Sustainable ecosystem powered by in-game
  advertisement, phygital merchandising and NFTs.
cover: .gitbook/assets/mxs_banner_nologo_1500_300.png
coverY: 0
layout:
  cover:
    visible: true
    size: hero
  title:
    visible: true
  description:
    visible: true
  tableOfContents:
    visible: true
  outline:
    visible: true
  pagination:
    visible: true
---

# MetaXSeed Games

MetaXSeed Games is a <mark style="color:red;">**layer 1 blockchain**</mark> and gaming studio with a <mark style="color:red;">**large library of mobile, web and PC games**</mark> that will use blockchain, NFTs, advertising and AI to unleash the next generation of sustainable 'play and earn' games. We want to combine the best of **Web2** (_fun games_, _free to play ad-driven model_) & **Web3** (_digital ownership_, _interoperability, transparency_). &#x20;

Using advertisement as a primary revenue source has two major advantages: \[Read more [here](metaxseed-games/advertisement.md)]

1. Keep all games <mark style="color:red;">**free**</mark> attracting a large player base
2. Provide a <mark style="color:red;">**reliable & consistent revenue stream**</mark> that can be used to reward players without the need to dilute the token supply

> _Web2 has over 3.2 billion gamers worldwide with CAGR of 12%. Only 0.09% of gamers have ever been involved in web3 gaming so the conversion potential here is limitless. \[_[_Ref_](https://www.ey.com/en\_ch/technology/does-in-game-advertising-have-the-potential-to-bring-in-the-eyeballs-and-the-ad-dollars)_]_
>
>
>
> _Web3 gaming sector is projected to grow at a rate of 70% for the next 5 years. \[_[_Ref_](https://www.marketsandmarkets.com/Market-Reports/blockchain-gaming-market-167926225.html)_]_

<figure><img src=".gitbook/assets/image (24).png" alt=""><figcaption></figcaption></figure>

We have published a range of games of various genres that are currently being tested on Steam, Google Play and our own web3 games launcher - <mark style="color:red;">**MetaXSeed Play**</mark> (<mark style="color:red;">PC, Mobile and Web</mark>).  Taking inspiration from projects such as Gala Games and Immutable X, our objective is to significantly increase token utility, enhance community engagement and create a balanced game-fi economy. &#x20;

<figure><img src=".gitbook/assets/image (1) (1) (1).png" alt=""><figcaption></figcaption></figure>

## <mark style="color:blue;background-color:blue;">XSEED Token</mark>

The primary ecosystem token will be <mark style="color:red;">**$XSEED**</mark> which will have a limited supply of 50 billion to match other comparable studios<mark style="color:red;">.</mark>  All games will <mark style="color:red;">**drive demand**</mark> for $XSEED. Token holders will have unprecedented utility as they will benefit from being a <mark style="color:red;">**currency**</mark> for the <mark style="color:red;">**game NFT**</mark>s, in-game <mark style="color:red;">**advertisement**</mark>, <mark style="color:red;">**governance, merchandising**</mark> and <mark style="color:red;">**node infrastructure**</mark>.\
\
$XSEED token <mark style="color:red;">**utilities**</mark> include:

* Gas fees for layer 1 blockchain
* Token utility within out vast library of games
* Purchase currency for ecosystem NFTs and node licences
* Purchase currency for in game advertisement injection
* Purchase currency for ecosystem merchandise
* Additional game-fi yield via staking, farming and lending
* DAO governance

For additional details, read the utility page [here](token-utility.md).

## <mark style="color:blue;background-color:blue;">Sector Comparables</mark>

We own the entire tech stack which includes the games, blockchain, launcher, marketplace and the monetization features. This will enable value extraction at each level and complete control of player engagement, diversified risk and ensure we are not dependent on third parties for future success. Our ‘low valuation’ and ‘high functionalities’ compare very positively with our primary competitors.

One of the main competitors would be Gala Games, which reached a market cap of $22 billion in 2021.  The potential for MetaXSeed Games is clear and it has the capability to be a major player in the web3 gaming industry.

<figure><img src=".gitbook/assets/image (25).png" alt=""><figcaption></figcaption></figure>

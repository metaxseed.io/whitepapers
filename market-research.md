---
cover: .gitbook/assets/mxs_banner_nologo_1500_300.png
coverY: 0
layout:
  cover:
    visible: true
    size: hero
  title:
    visible: true
  description:
    visible: true
  tableOfContents:
    visible: true
  outline:
    visible: true
  pagination:
    visible: true
---

# Market Research

<figure><img src=".gitbook/assets/2.png" alt=""><figcaption></figcaption></figure>

From the massive arcade explosion in the 80’s to the rise of home consoles and PC gaming in the 90’s, the games industry has rapidly grown to rival and outpace film and television as the primary source of entertainment.  Once mobile gaming took off, that wave of revenue generation potential became an ever growing tsunami.

The diagram below shows the gaming market revenue has grown over time. For reference, VR gaming is the tiny pink slither on the top right which illustrates the immense potential of the sector we are entering. P2E combined with NFTs and Metaverse is very much the future of the industry as some of the largest companies in the world look to enter the sector.

![](<.gitbook/assets/Screenshot 2022-02-19 003017.png>)

<figure><img src=".gitbook/assets/3.png" alt=""><figcaption></figcaption></figure>

Play-to-earn financial model lets the gamers create new digital assets and trade them via the game infrastructure.  Using the play-to-earn concept, players can earn virtual in-game currency which is liquid and can be quite easily sold for other cryptocurrencies and fiat.

Blockchain gaming is currently being adopted at an increasingly rate.  There is increasing evidence that gaming is the number one use case for crypto currency above and beyond de-fi.

Referencing the most recent [Blockchain Game Report](https://dappradar.com/blog/bga-blockchain-game-report-july-2021) from the Blockchain Game Alliance (BGA) and other sources, over 750k unique active users connected to blockchain-based games in July 2021   That trend has only accelerated coming into 2022 & 2023.

![Blockchain-based gaming is seeing a boom that's outpacing NFTs and DeFi.  Source = DappRadar](<.gitbook/assets/401778b0-004b-11ec-a337-ec1d9d808cb8 (1).webp>)

![Blockchain-based gaming is seeing a boom that's outpacing NFTs and DeFi, and is linked to increased usage of social platforms.
Source = DappRadar](<.gitbook/assets/7b014230-004b-11ec-befe-95a6c0ace28b (1).webp>)

![Google searches for Axie Infinity, a blockchain-based game, have spiked. Source = DappRadar](<.gitbook/assets/c39594b0-004b-11ec-bb91-4af57ec26bfe (1).webp>)

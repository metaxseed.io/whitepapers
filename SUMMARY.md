# Table of contents

* [MetaXSeed Games](README.md)
  * [XSeed Chain](xseed-chain.md)
  * [Games Library](metaxseed-games/games-library.md)
  * [Games Launcher](metaxseed-games/games-launcher.md)
  * [NFT Marketplace](metaxseed-games/nft-marketplace.md)
  * [Advertisement](metaxseed-games/advertisement.md)
  * [Phygital Merchandise](metaxseed-games/phygital-merchandise.md)
* [Token Utility](token-utility.md)
* [Problems Being Tackled](problems-being-tackled.md)
* [Market Research](market-research.md)
* [Business Model](business-model/README.md)
  * [Collaborative Approach](business-model/collaborative-approach.md)
  * [Diverse Revenue Streams](business-model/diverse-revenue-streams/README.md)
    * [Monetization](business-model/diverse-revenue-streams/monetization.md)
* [In-Game Advertising](in-game-advertising.md)
* [Unreal Engine 5](unreal-engine-5.md)
* [Digital Assets - NFTs](digital-assets-nfts.md)
* [Our Games Pipeline](our-games-pipeline.md)
* [Roadmap](roadmap.md)
* [Tokenomics](tokenomics.md)
* [Team and Partners](team-and-partners.md)
* [Social Media](social-media.md)

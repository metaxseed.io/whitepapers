---
cover: .gitbook/assets/mxs_banner_nologo_1500_300.png
coverY: 0
layout:
  cover:
    visible: true
    size: hero
  title:
    visible: true
  description:
    visible: true
  tableOfContents:
    visible: true
  outline:
    visible: true
  pagination:
    visible: true
---

# Social Media

Join our community channels to stay up to date

<div data-full-width="false">

<figure><img src=".gitbook/assets/linktree.png" alt=""><figcaption><p><a href="https://linktr.ee/metaxseed">https://linktr.ee/metaxseed</a></p></figcaption></figure>

</div>

Click here to open a list of all of our important links - social media, games, blockchain, marketplace, shop, github, etc.

## [https://linktr.ee/metaxseed](https://linktr.ee/metaxseed)

